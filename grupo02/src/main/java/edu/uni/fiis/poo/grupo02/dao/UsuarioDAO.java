package edu.uni.fiis.poo.grupo02.dao;

import edu.uni.fiis.poo.grupo02.bean.Usuario;

import java.util.List;

public interface UsuarioDAO {
    public List<Usuario> mostrarUsuarios() throws Exception;
    public int mostrarCantidadUsuarios() throws Exception;
    public void agregarUsuario(Usuario usuario) throws Exception;
    public void eliminarUsuario(String correousuario) throws Exception;
    public String buscarUsuario(String correousuario) throws Exception;
}
