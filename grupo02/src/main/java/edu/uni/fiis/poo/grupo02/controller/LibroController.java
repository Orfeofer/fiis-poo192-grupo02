package edu.uni.fiis.poo.grupo02.controller;

import edu.uni.fiis.poo.grupo02.bean.Libro;
import edu.uni.fiis.poo.grupo02.request.LibroRequest;
import edu.uni.fiis.poo.grupo02.service.LibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LibroController {
    @Autowired
    LibroService libroService;

    @RequestMapping("/libros")
    public List<Libro> mostrarLibros() throws Exception{
        return libroService.mostrarLibros();
    }

    @RequestMapping("/libros/cantidad")
    public String mostrarCantidadLibros() throws Exception{
        return "<h1> Tenemos " +  libroService.mostrarCantidadLibros() + " libros registrados </h1>";
    }

    @PostMapping("/libros/agregar")
    public String agregarLibro(@RequestBody LibroRequest req) throws Exception{
        Libro libro = new Libro(req.getCodigolibro(),req.getNombrelibro(),req.getAutor(),
                                req.getPrecio(),req.getEditorial(),req.getEdicion(),
                                req.getCategoria(),req.getCategoria(),req.getResumen());
        libroService.agregarLibro(libro);
        return "Libro registrado con éxito";
    }

    @PostMapping("/libros/eliminar")
    public String eliminarLibro(@RequestParam String codigolibro) throws Exception{
        Libro libro = new Libro(codigolibro);
        libroService.eliminarLibro(codigolibro);
        return "<h1> Libro eliminado de la base de datos </h1>";
    }

    @RequestMapping("/libros/buscar")
    public String buscarLibro(@RequestParam String codigolibro) throws Exception{
        return "<h1> " + libroService.buscarLibro(codigolibro) + "</h1>";
    }

}
