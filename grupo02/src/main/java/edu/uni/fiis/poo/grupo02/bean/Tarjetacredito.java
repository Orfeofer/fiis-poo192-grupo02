package edu.uni.fiis.poo.grupo02.bean;

public class Tarjetacredito {
    private String numerotarjeta;
    private String nombreusuario;
    private String correousuario;
    private String cvc;

    public Tarjetacredito(String numerotarjeta, String nombreusuario, String correousuario, String cvc){
        this.numerotarjeta = numerotarjeta;
        this.nombreusuario = nombreusuario;
        this.correousuario = correousuario;
        this.cvc = cvc;
    }

    public Tarjetacredito(String numerotarjeta){
        this.numerotarjeta = numerotarjeta;
    }

    public String getNumerotarjeta() {
        return numerotarjeta;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public String getCorreousuario() {
        return correousuario;
    }

    public String getCvc() {
        return cvc;
    }
}
