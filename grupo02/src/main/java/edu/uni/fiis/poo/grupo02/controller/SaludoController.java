package edu.uni.fiis.poo.grupo02.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SaludoController {
    @RequestMapping("/saludo")
    public String saludar(){
        return "<h1> Bienvenido a VirtualBook </h1>";
    }
}
